﻿using UnityEngine;
using System.Collections;

public class Stein : MonoBehaviour {
    public float damage = 10;
    public GameObject shooter;
    private bool hit = false;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //transform.Translate(transform.right);
	}

    void OnCollisionEnter(Collision col)
    {
        //if (!hit)
        //{
            
            //Debug.Log("I hit " + col.gameObject.name);

            LivingEntity npc = col.gameObject.GetComponent<LivingEntity>();

            if (npc != null && GetComponent<BrainBase>() != null && GetComponent<BrainBase>().squad != col.gameObject.GetComponent<BrainBase>().squad)
            {
                npc.Damage(10, (int)(DAMAGE_TYPES.TYPE_PHYSICAL | DAMAGE_TYPES.TYPE_POISON | DAMAGE_TYPES.TYPE_BLEED), shooter);
            }
            //hit = true;
        //}
    }
}
