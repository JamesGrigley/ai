﻿using UnityEngine;
using System.Collections;

public class CompanionManager : MonoBehaviour {

    Squad squad;
    public GameObject waypointIcon;

    private float currentForwardValue = 3;
    private GameObject waypointInstance = null;

    // Use this for initialization
	void Start () {
        squad = GetComponent<BrainBase>().squad;

    }
	
	// Update is called once per frame
	void Update () {
        // first pressed, instiate
        if (Input.GetKeyDown(KeyCode.G))
        {
            //Debug.Log("Key Down");
            waypointInstance = (GameObject)Instantiate(waypointIcon, (transform.position + transform.forward * currentForwardValue), transform.parent.rotation);

            //Debug.Log("waypointInstance is init at " + waypointInstance.transform.position);
        }

        // held, continue to show icon
        if (Input.GetKey(KeyCode.G) && waypointInstance.transform.position != null)
        {
            //Debug.Log("Key Held");
            waypointInstance.transform.position = transform.position + transform.forward * currentForwardValue;
        }

        
        if (Input.GetKeyUp(KeyCode.G))
        {
            //Debug.Log("Key Up");
            squad.receiveCommand("charge", waypointInstance.transform.position, GetComponent<BrainBase>());
            Destroy(waypointInstance);
        }

        if (Input.GetMouseButtonDown(1) && Input.GetKey(KeyCode.G))
        {
            //Debug.Log("mouse");
            currentForwardValue += 2;
        }
    }


}
