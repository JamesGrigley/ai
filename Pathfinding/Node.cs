﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : IHeapItem<Node>  {
	
	public bool walkable;
	public Vector3 worldPosition;
	public int gridX;
	public int gridY;
    public int gridZ;

	public int gCost;
	public int hCost;

    int heapIndex;
    public bool extended = false;

    public List<Node> neighborNodes;
    public Node parentNode;
	
	public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY, int _gridZ) {

        neighborNodes = new List<Node>();

        walkable = _walkable;
		worldPosition = _worldPos;
		gridX = _gridX;
		gridY = _gridY;
        gridZ = _gridZ;
	}

	public int fCost {
		get {
			return gCost + hCost;
		}
	}

    public Vector3 gridVector
    {
        get
        {
            return new Vector3(gridX, gridY, gridZ);
        }
    }

    public Vector3 costVector
    {
        get
        {
            return new Vector3(gCost, hCost, fCost);
        }
    }

	public int HeapIndex {
		get {
			return heapIndex;
		}
		set {
			heapIndex = value;
		}
	}

    public bool Extended
    {
        get
        {
            return extended;
        }
        set
        {
            extended = value;
        }
    }

	public int CompareTo(Node nodeToCompare) {
        //if (nodeToCompare.extended) // already extended nodes should fall to bottom
        //    return 0;

		int compare = fCost.CompareTo(nodeToCompare.fCost);
		if (compare == 0) {
			compare = hCost.CompareTo(nodeToCompare.hCost);
		}
		return compare;
	}

    public Node getBestNeighbor()
    {
        return parentNode;

        //Node bestNeighbor = null;

        //foreach (Node node in neighborNodes)
        //{
        //    if (bestNeighbor == null || node.gCost < bestNeighbor.gCost)
        //    {
        //        bestNeighbor = node;
        //    }
        //}

        //// remove the parent connection so it doesn't trace backwards
        ////bestNeighbor.neighborNodes.Remove(this);
        //neighborNodes.Remove(bestNeighbor);

        //return bestNeighbor;
    }

    //void OnGUI()
    //{
    //    if (walkable)
    //    {
    //        var point = Camera.main.WorldToScreenPoint(worldPosition);
    //        GUI.Label(new Rect(point.x, Screen.currentResolution.height - point.y - 200, 200, 200), heapIndex.ToString());
    //    }
    //}
}
