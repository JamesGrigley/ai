﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public abstract class SensorBase : MonoBehaviour {

    public float senseRadius = 50;
    public float senseDelay = 1;
    private float lastSense;
    public string senseName = "";

    private bool isSensing = false;

    int LayerID;

    protected int layerMask;

    protected SenseManager senseManager;

	void Start () {
        LayerID = LayerMask.NameToLayer("Combat");
        layerMask = 1 << LayerID;
        lastSense = Time.time - senseDelay;
        senseManager = GetComponent<SenseManager>();

	}

    void Update()
    {
        sense();
    }

    private void sense()
    {
        if (!isSensing && Time.time - lastSense >= senseDelay)
        {
            isSensing = true;
            senseNewObjects();
            //Debug.Log(gameObject.name + ": Sense from " + senseName);
            lastSense = Time.time;
            isSensing = false;
        }
    }

    protected abstract void senseNewObjects();

}
