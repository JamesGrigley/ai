﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBrain : BrainBase
{

    // our senses are alerting us to an object
    public override void receiveAlertFromSenses(GameObject alertGameObject)
    {
        //Debug.Log("PlayerBrain.receiveAlertFromSquad");
        squad.alert(alertGameObject, getPosition(), (IBrain)this);
    }

    // our squad is alerting us to an object
    public override void receiveAlertFromSquad(GameObject alertGameObject)
    {
       // Debug.Log("PlayerBrain.receiveAlertFromSquad");
        senseManager.receiveAlert(alertGameObject);
    }

    public override void receiveCommand(string command, Vector3 target)
    {
        Debug.Log("Fuck you I won't do you what you tell me.");
    }

    
}
