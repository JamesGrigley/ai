﻿using UnityEngine;
using System.Collections;

public class IdleAction : GoapAction {

    private bool isIdle = false;
    private bool doneIdle = false;

    public IdleAction()
    {
        actionName = "Idle";

        addEffect("idle", true);
	}

    public override void reset()
    {
        doneIdle = false;
        isIdle = false;
    }

    public override bool isDone()
    {
        return doneIdle;
    }

    public override bool requiresInRange()
    {
        return false;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        return true;
    }

    public override bool perform(GameObject agent)
    {
        if (!isIdle)
            StartCoroutine("beIdle");
        return true;
    }

    IEnumerator beIdle()
    {
        isIdle = true;
        yield return new WaitForSeconds(10);
        doneIdle = true;
    }
}
