﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

    public Transform bullet;

    private float shotDelay = 0.1f;
    private float lastShot = Mathf.NegativeInfinity;
    private bool shooting = false;

    public FireModeType fireMode = FireModeType.SemiAuto;

    public enum FireModeType
    {
        SemiAuto = 1,
        Auto = 2
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if ((fireMode == FireModeType.SemiAuto && Input.GetMouseButtonDown(0) || (fireMode == FireModeType.Auto && Input.GetMouseButton(0))))
        {
            shoot();
        }
	}

    private void shoot()
    {
        if (Time.time > (lastShot + shotDelay))
        {
            shooting = true;
            blowBack();
            //GetComponent<AudioSource>().Play();
            Transform barrelHole = transform.Find("BarrelHole");
            Transform bulletInstance = Instantiate(bullet, (transform.parent.position + transform.parent.forward * 2), transform.parent.rotation) as Transform;
            bulletInstance.GetComponent<Bullet>().shooter = this.transform.parent.gameObject;

            lastShot = Time.time;
            //Debug.Log ("shot");

        }
    }

    private void blowBack()
    {
        transform.localPosition += Vector3.back * 0.05f;
        StartCoroutine(blowBackreturn());
    }

    IEnumerator blowBackreturn()
    {
        yield return new WaitForSeconds(0.05f);
        //Debug.Log ("blowBackreturn");

        transform.localPosition += Vector3.forward * 0.05f;
    }
}
