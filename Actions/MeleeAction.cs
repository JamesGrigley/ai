﻿using UnityEngine;
using System.Collections;

public class MeleeAction : GoapAction {

    private bool swung = false;

    public SenseManager sensor;

    private NPCController npcController;


    private int swingState = 0;
    private float swingStartTime;
    private float swingDuration = 1f;

    public MeleeAction()
    {
        actionName = "MeleeAction";

        addPrecondition("canSeeTarget", true);
        addPrecondition("isSafe", true);
        addPrecondition("canMelee", true);

        addEffect("killTarget", true);
	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
        npcController = GetComponent<NPCController>();
    }

    void Update()
    {
        handleSwing();
    }


    public override void reset()
    {
        //Destroy(target);
        swung = false;
    }

    public override bool isDone()
    {
        return swung;
    }

    public override bool requiresInRange()
    {
        return true;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        //Debug.Log(gameObject.name + ": Shoot checkProceduralPrecondition");

        SensedObject sensedTarget = sensor.getClosestKnownTarget(false); // we don't need to see them yet

        if (sensedTarget != null)
            target = sensedTarget;

        //return target != null;
        return true;
    }

    public override bool perform(GameObject agent)
    {
        Debug.Log(gameObject.name + ": melee perform");
        return swing();
    }

    private bool swing()
    {
        bool inRange = isInRange();

        if (inRange && swingState == 0)
        {
            // only look on x/y axis
            Vector3 lookatVector = new Vector3(target.sensedObject.transform.position.x, transform.position.y, target.sensedObject.transform.position.z);

            transform.LookAt(lookatVector);
            swingStartTime = Time.time;
            swingState = 1;

        }
        //Debug.Log(gameObject.name + ": start swing");

        // end swing if target moves away? fix this
        if (!inRange && swingState > 0)
        {
            swung = true;
        }

        return inRange;
    }

    private void handleSwing()
    {
       // Debug.Log("handleSwing");
        if (swingState == 2 && (swingStartTime + swingDuration) <= Time.time)
        {
            swingState = 0;
            npcController.animateStop();

        }
        else if (swingState == 1)
        {
            swingState = 2;
            npcController.animateMelee();
        }

    }
}
