﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SightSensor : SensorBase {

    public float sightAngle = 60;
    public float sightDistance = 20;

    protected override void senseNewObjects()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, senseRadius, layerMask);
        
        RaycastHit hit;
        Vector3 raycastDirection;
        Vector3 raycastOrigin;

        foreach (Collider col in cols)
        {
            //the angle code looks funny, but Vector3.Angle transform to transform was producing
            //odd results that apparenlty had something to do with world position 0 0 0
            //found use with same issue, he did this to fix
            Vector3 toVector = col.transform.position - transform.position;
            float angleToTarget = Vector3.Angle(transform.forward, toVector);

            //Debug.Log(gameObject.name + ": " + col.gameObject.name + " at " + angleToTarget + " degrees");

            // if within sight angle and can hit with a raycast
            // check sight radius
            if (angleToTarget <= sightAngle)
            {

                raycastDirection = col.transform.position - transform.position;
                raycastOrigin = transform.position + Vector3.up;

                //Debug.DrawRay(raycastOrigin, raycastDirection, Color.red);

                if (Physics.Raycast(raycastOrigin, raycastDirection, out hit, senseRadius))
                {
                    //Debug.Log("Raycast hit: " + hit.transform.name);
                    if (hit.transform.name == col.transform.name)
                    {
                        //Debug.DrawRay(raycastOrigin, raycastDirection, Color.green);
                        //Debug.Log("I see " + col.gameObject.name);
                        if (senseManager.updateSensedObject(col.gameObject, true))
                        {
                            //Debug.Log(gameObject.name + ": Oh look I can see " + col.gameObject.name);
                        }
                    }
                    else
                    {
                        //Debug.Log("I was trying to hit " + col.transform.name + " but hit " + hit.transform.name + " instead");
                    }
                }
            }
        }
    }
}
