using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class BrainBase : MonoBehaviour, IBrain
{
    public Squad squad; // this is so brain can talk to SM

    public SenseManager senseManager; // so brain can tell sensor about alerts received from SM
    
    public bool isAlpha = false;

	void Awake ()
	{
        senseManager = GetComponent<SenseManager>();
	}

    public abstract void receiveAlertFromSenses(GameObject alertGameObject);

    public abstract void receiveAlertFromSquad(GameObject alertGameObject);

    public abstract void receiveCommand(string command, Vector3 target);

    
    public void assignSquad(Squad _squad)
    {
        //Debug.Log(_squad.name + " squad.");
        squad = _squad;
    }

    public Vector3 getPosition()
    {
        return gameObject.transform.position;
    }
}

