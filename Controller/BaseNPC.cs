﻿using UnityEngine;
using System.Collections;
using System;

public class BaseNPC : BaseCharacter
{
    protected NavMeshAgent navMeshAgent;
    protected GameObject attackingTarget = null;
    protected CombatNodes combatNodes;
    protected Vector3 destination;
    protected bool isAttacking = false;

    protected float attackDistance = 2;
    protected float standDistaince = 2;

    protected float walkSpeed = 1;

    protected bool isHostile = false;

    NavMeshPath path;
    bool previewPath = false;
    float elapsed = 0.0f;

    public delegate void GotoAndPerformDelegate();

    GotoAndPerformDelegate gotoAndPerformDelegate = null;

    protected override void Awake()
    {
        base.Awake();
        navMeshAgent = GetComponent<NavMeshAgent>();
        destination = navMeshAgent.destination;
        path = new NavMeshPath();
    }

    public override void Start()
    {
        base.Start();
    }


    void Update()
    {
        if (attackingTarget != null)
        {
            if (!isAttacking && Vector3.Distance(transform.position, attackingTarget.transform.position) <= attackDistance)
            {
                navMeshAgent.speed = 0;
                isAttacking = true;
                attack();

            }
            else if(Vector3.Distance(destination, transform.position) > attackDistance)
            {
                navMeshAgent.speed = walkSpeed;
                setCombatDestination();
            }

            if (previewPath)
            {
                elapsed += Time.deltaTime;
                if (elapsed > 1.0f)
                {
                    elapsed -= 1.0f;
                    NavMesh.CalculatePath(transform.position, attackingTarget.transform.position, NavMesh.AllAreas, path);
                }
                for (int i = 0; i < path.corners.Length - 1; i++)
                    Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
            }
        }

        if (walkSpeed > 0 && Vector3.Distance(transform.position, destination) <= standDistaince)
        {
            stop();
        }
    }

    protected virtual void alert(bool canSee = false) { }

    public override void stop()
    {
        navMeshAgent.speed = 0;
        animator.SetFloat("Speed", 0);
        if (gotoAndPerformDelegate != null)
        {
            gotoAndPerformDelegate();
            gotoAndPerformDelegate = null;
        }
    }

    public void senseTarget(GameObject alertObject, bool canSee = false)
    {
        //Debug.Log("Sensed target");

        alert(canSee);

        if (isHostile)
        {
            attackingTarget = alertObject;
            combatNodes = attackingTarget.GetComponent<CombatNodes>();
            setCombatDestination();
        }
    }

    void setCombatDestination()
    {
        destination = combatNodes.getClosestNode(transform);
        navMeshAgent.SetDestination(destination);
        animator.SetFloat("Speed", walkSpeed);
    }

    public void setDesitination(Transform target)
    {
        destination = target.position;
        navMeshAgent.SetDestination(destination);
        animator.SetFloat("Speed", walkSpeed);
    }

    public void gotoAndPerform(Transform target, GotoAndPerformDelegate _gotoAndPerformDelegate)
    {
        setDesitination(target);
        gotoAndPerformDelegate = _gotoAndPerformDelegate;
    }

    public void showPath()
    {
        previewPath = true;
    }

}

