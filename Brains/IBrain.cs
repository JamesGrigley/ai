﻿using UnityEngine;
using System.Collections;

public interface IBrain {


    void assignSquad(Squad _squad);

    void receiveAlertFromSquad(GameObject alertGameObject);
    void receiveCommand(string command, Vector3 target);
    Vector3 getPosition();
}
