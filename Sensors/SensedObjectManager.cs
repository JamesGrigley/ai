﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SensedObjectManager : MonoBehaviour
{
    public List<SensedObject> sensedObjects;
    List<SensedObject> sensedObjectsToRemove;
    private SenseManager senseManager;

    private float memoryTime = 10;
    private float sightMemoryTime = 1;

    void Awake()
    {
        sensedObjectsToRemove = new List<SensedObject>();
        senseManager = GetComponent<SenseManager>();
        sensedObjects = new List<SensedObject>();
    }

    void Update()
    {
        updateMemory();
    }

    public bool Add(GameObject sensedObject)
    {
        return Add(sensedObject, false);
    }

    public bool Add(GameObject sensedObject, bool canSee)
    {
//        Debug.Log(gameObject.name + ": Adding " + sensedObject.name + ", canSee:" + canSee.ToString());

        // i really need to clean this up

        bool foundObject = false;
        bool isNewSense = false;

        SensedObject alertObject = null;

        // if I already know about the object then update it
        foreach (SensedObject so in sensedObjects)
        {
            //Debug.Log("in sensened objects");
            if (so.sensedObject == sensedObject)
            {
                so.knownPosition = sensedObject.transform.position;
                so.canSee = canSee; 
                
                if (so.triggerAlert)
                {
                    alertObject = so;
                    so.triggerAlert = false;
                }
                //Debug.Log(gameObject.name + ": Updating " + sensedObject.name + ", canSee:" + canSee.ToString());
                isNewSense = false; // not a new sense
                foundObject = true;
            }
        }

        if (!foundObject)
        {
            // I didn't find an existing object to update, so add one
            //Debug.Log(gameObject.name + ": added " + sensedObject.name + ", canSee:" + canSee.ToString());

            SensedObject newSensedObject = new SensedObject(sensedObject, canSee);
            
            if (newSensedObject.triggerAlert){
                alertObject = newSensedObject;
                newSensedObject.triggerAlert = false;
            }

            sensedObjects.Add(newSensedObject);
            isNewSense = true;


        }
        
        if (alertObject != null)
        {
            senseManager.receiveAlertFromOM(sensedObject, canSee);
        }

        return isNewSense; // this was a new sense
    }

    public SensedObject getClosestKnownTarget(bool canSee = false)
    {
        // loop through the known objects and return the closest one
        SensedObject closestObject = null;

        foreach (SensedObject sensedObject in sensedObjects)
        {
            //Debug.Log("getClosestKnownGameObject LOOP");

            if (sensedObject != null
                && sensedObject.sensedObject != null
                && (canSee == false || sensedObject.canSee == true)
                && (closestObject == null || Vector3.Distance(transform.position, sensedObject.sensedObject.transform.position) < Vector3.Distance(transform.position, closestObject.sensedObject.transform.position))
                )
            {
                //Debug.Log(gameObject.name + ": found closest object " + sensedObject.sensedObject.name);

                closestObject = sensedObject;
            }
        }
        return closestObject;
    }

    public Vector3 getFleeDirection()
    {
        int targetCount = 0;
        Vector3 totalDirection = new Vector3();
        Vector3 fleeDirection = new Vector3();

        foreach (SensedObject sensedObject in sensedObjects)
        {
            if (sensedObject != null
                && sensedObject.sensedObject != null
                )
            {
                targetCount++;
                totalDirection += sensedObject.sensedObject.transform.position - transform.position;
            }
        }

        if (targetCount > 0){
            fleeDirection = (totalDirection / targetCount);
        }


        return fleeDirection * -1;
    }

    public bool knowsTarget(GameObject target)
    {
        foreach (SensedObject sensedObject in sensedObjects)
        {
            if (sensedObject != null && sensedObject.sensedObject != null && sensedObject.sensedObject.name == target.name)
            {
                return true;
            }
        }
        return false;
    }

    public bool canSeeTarget(GameObject target)
    {
        foreach (SensedObject sensedObject in sensedObjects){
            if (sensedObject != null && sensedObject.sensedObject != null && sensedObject.sensedObject.name == target.name && sensedObject.canSee)
            {
                return true;
            }
        }
        return false;
    }

    private void updateMemory()
    {
        //return; //fix this
        

        foreach (SensedObject so in sensedObjects)
        {
            // if the object was destroyed, or if memory time lapsed (forgot about target)
            if (so == null || Time.time - so.knowTime > memoryTime)
            {
                sensedObjectsToRemove.Add(so);
            }
                // lose sight of objects
            else if (so.canSee == true && Time.time - so.sightTime > sightMemoryTime)
            {
                //Debug.Log(gameObject.name + ": OMFG where did he go?");
                senseManager.updateSensedObject(so.sensedObject, false);
                so.triggerAlert = true;
            }
        }

        foreach (SensedObject so in sensedObjectsToRemove)
        {
            if (sensedObjects.Contains(so))
            {
                sensedObjects.Remove(so);
            }
        }
    }
}