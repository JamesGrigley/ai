﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FindCheeseAction : GoapAction {

    private bool foundCheese = false;
    public float scanRadius = 5;

    private SenseManager sensor;

    public FindCheeseAction()
    {
        actionName = "FindCheeseAction";

        addPrecondition("isSafe", true);
        addEffect("findCheese", true);
	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
    }

    public override void reset()
    {
        //Debug.Log("resetting");
        Destroy(target.sensedObject);
        foundCheese = false;
    }

    public override bool isDone()
    {
        return foundCheese;
    }

    public override bool requiresInRange()
    {
        return true;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        return findNextCheese();
    }
    
    public override bool perform(GameObject agent)
    {
        if (target.sensedObject.name.ToLower().Contains("cheese"))
        {
            GetComponent<EatCheeseAction>().target = target;
        }
        foundCheese = true;
        return true;
    }

    private bool findNextCheese()
    {
        //Debug.Log("where da cheeeese at?");

        //GameObject cheese = sensor.getClosestKnownTarget().sensedObject;

        //if (cheese == null)
        //{
        //    //Debug.Log("I don't know where the cheese at :(");
        //    setRandomPosition();
        //}
        //else
        //{
        //    //Debug.Log("I know where cheese is!");
        //    target = cheese;
        //}
        return target != null;
    }

    private void setRandomPosition()
    {
        ////Debug.Log("getRandomPosition");
        //transform.rotation = transform.rotation * Quaternion.Euler(0, Random.Range(0, 360), 0);

        //target = new GameObject("Random Position");
        //target.transform.position = transform.position + (transform.forward * 10);

        //target.transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);

    }

}
