﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatNodes : MonoBehaviour
{
    List<CombatNode> combatNodes;

    void Awake()
    {
        combatNodes = new List<CombatNode>();

        Transform combatNodeTransform = transform.FindChild("CombatNodes");

        foreach (CombatNode node in combatNodeTransform.GetComponentsInChildren<CombatNode>())
        {
            combatNodes.Add(node);
        }

    }

    public Vector3 getClosestNode(Transform attacker)
    {
        return attacker.position;
        CombatNode closestNode = getFrontNode();

        if (closestNode == null)
            foreach (CombatNode node in combatNodes)
            {
                if (node.attacker == attacker)
                {
                    node.attacker = null;
                }

                if (node.attacker == null && ( closestNode == null || Vector3.Distance(node.position, attacker.position) < Vector3.Distance(closestNode.position, attacker.position)))
                {
                    closestNode = node;

                }
            }
        closestNode.attacker = attacker;
        return closestNode.position;
    }

    CombatNode getFrontNode()
    {
        foreach (CombatNode node in combatNodes)
        {
            if (node.isFront && node.attacker == null)
                return node;
        }
        return null;
    }

    void Update()
    {
        foreach(CombatNode node in combatNodes)
        {
            Debug.DrawLine(transform.position, node.position, Color.red);
        }
    }


}
