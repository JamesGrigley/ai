﻿using UnityEngine;
using System.Collections;

public class PursueAction : GoapAction {

    private bool canSeeTarget = false;
    private SenseManager sensor;

    public PursueAction()
    {
        actionName = "PursueAction";

        addPrecondition("knowsTarget", true);
        addPrecondition("canSeeTarget", false);

        addEffect("canSeeTarget", true);

	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
    }

    public override void reset()
    {

        canSeeTarget = false;
    }

    public override bool isDone()
    {
        return canSeeTarget;
    }

    public override bool requiresInRange()
    {
        return true; 
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {

        target = sensor.getClosestKnownTarget();
        //Debug.Log(gameObject.name + ": persue checkProceduralPrecondition target != null: " + (target != null).ToString());
        return target != null;
    }

    public override bool perform(GameObject agent)
    {
        //Debug.Log(gameObject.name + ": persue perform");

        if (target.sensedObject != null)
        {
            transform.LookAt(target.sensedObject.transform.position);
            if (sensor.canSeeTarget(target.sensedObject))
            {
                canSeeTarget = true;
            }
            return true;
        }
        return false;
    }
}
