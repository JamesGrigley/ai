﻿using UnityEngine;
using System.Collections;

public class AIDebug : MonoBehaviour {

    GoapAgent agent;
    NPCController npcController;
    GUIStyle guiStyle;

	// Use this for initialization
	void Start () {
        agent = GetComponent<GoapAgent>();
        npcController = GetComponent<NPCController>();
        guiStyle = new GUIStyle();
         
        guiStyle.alignment = TextAnchor.UpperLeft;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        GUI.backgroundColor = Color.gray;
        GUI.Box(new Rect(Screen.width - 320, 20, 300, 200), 
            "AI State: " + agent.debugCurrentState + "\r\n" +
            "Is Moving: " + npcController.isMoving.ToString() + "\r\n" +
            "Path: " + npcController.debugPath + "\r\n"
            , guiStyle);
    }
}
