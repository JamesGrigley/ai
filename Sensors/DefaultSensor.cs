﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DefaultSensor : SensorBase {

    protected override void senseNewObjects()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, senseRadius, layerMask);
        foreach (Collider col in cols)
        {
            //Debug.Log("Oh look I found " + col.gameObject.name + " on layerid " + LayerID.ToString());
            if (senseManager.updateSensedObject(col.gameObject))
            {

            }
            else
            {
                //Debug.Log("I already knew about " + col.gameObject.name);
            }
        }
    }
}
