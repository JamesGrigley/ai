﻿using UnityEngine;
using System.Collections;

public abstract class NPCController : MonoBehaviour
{
    public float moveSpeed = 4;
    private Vector3[] path;
    public bool isMoving = false;
    public string debugPath = "Initialized";


    float lastWaypointRequest = 0;
    float delayWaypointRequest = 3;

    private CharacterController controller;
    protected Animator animator;

    // Use this for initialization
    void Awake()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    public virtual void animateIdle() { }
    public virtual void animateMove() { }
    public virtual void animateStop() { }
    public virtual void animateMelee() { }

    public void moveTo(Vector3 targetPosition)
    {
        if (!isMoving && (lastWaypointRequest == 0 || Time.time - lastWaypointRequest >= delayWaypointRequest))
        {
            isMoving = true;
            lastWaypointRequest = Time.time;

            PathRequestManager.RequestPath(transform.position, targetPosition, OnPathFound);
        }
    }

    public void stop()
    {
        //Debug.Log("Stop agent");
        StopCoroutine("FollowPath");
        isMoving = false;
        animateStop();

    }

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {

        //Debug.Log("On path found. pathSuccessful:" + pathSuccessful.ToString());
        if (pathSuccessful)
        {
            path = newPath;
            StopCoroutine(FollowPath()); // shouldn't need this with isMoving flag
            StartCoroutine(FollowPath());
        }
        else
        {
            debugPath = "Failed to find path";
            stop();
        }
    }

    IEnumerator FollowPath()
    {

        if (path.Length < 1 || path[0] == null)
        {
            Debug.Log("Returned no paths");
            yield break;
        }

        Vector3 currentWaypoint = path[0];
        int targetIndex = 0;

        animateMove();
        while (isMoving)
        {
            //Debug.Log(gameObject.name + ": I'm moving!");

            if (Vector3.Distance(transform.position, currentWaypoint) <= 2f)
            {
                targetIndex++;
                if (targetIndex >= path.Length)
                {
                    Debug.Log(gameObject.name + ": FollowPath ending");

                    isMoving = false;
                    animateStop();

                    yield break;

                }
                currentWaypoint = path[targetIndex];
            }

            move(currentWaypoint);

            yield return null;
        }

    }

    private void move(Vector3 currentWaypoint)
    {
        transform.LookAt(new Vector3(currentWaypoint.x, transform.position.y, currentWaypoint.z));

        Vector3 forward = transform.TransformDirection(Vector3.forward);

        controller.SimpleMove(forward * moveSpeed);
    }
}
