
using UnityEngine;
using System.Collections.Generic;
using System;

public abstract class GoapAction : MonoBehaviour {


	private HashSet<KeyValuePair<string,object>> preconditions;
	private HashSet<KeyValuePair<string,object>> effects;

	private bool inRange = false;
    public string actionName = "Goap Action";

    public float inRangeProximty = 0;

	/* The cost of performing the action. 
	 * Figure out a weight that suits the action. 
	 * Changing it will affect what actions are chosen during planning.*/
	public float cost = 1f;

	/**
	 * An action often has to perform on an object. This is that object. Can be null. */
	public SensedObject target;
    public bool requiresObjectTarget = true;
    public Vector3 nonObjectTarget;

    public Vector3 targetPosition
    {
        get
        {
            if (requiresInRange() && requiresObjectTarget && (actionName == "FollowAction" || target.canSee))
            {
                // Only move to target's exact position if the action requires target to be in range AND we can see it, or if it is follow

                //Debug.Log(nextAction.target.sensedObject.name);
                Renderer renderer = target.sensedObject.GetComponent<Renderer>();
                
                return target.sensedObject.transform.position - renderer.bounds.extents;
            }
            else if (!requiresObjectTarget)
            {
                return nonObjectTarget;
            }
            else
            {
                return target.knownPosition;
            }
        }
    }

	public GoapAction() {
		preconditions = new HashSet<KeyValuePair<string, object>> ();
		effects = new HashSet<KeyValuePair<string, object>> ();
	}

	public void doReset() {
		//inRange = false;
		//target = null;
		reset ();
	}

	/**
	 * Reset any variables that need to be reset before planning happens again.
	 */
	public abstract void reset();

	/**
	 * Is the action done?
	 */
	public abstract bool isDone();

	/**
	 * Procedurally check if this action can run. Not all actions
	 * will need this, but some might.
	 */
	public abstract bool checkProceduralPrecondition(GameObject agent);

	/**
	 * Run the action.
	 * Returns True if the action performed successfully or false
	 * if something happened and it can no longer perform. In this case
	 * the action queue should clear out and the goal cannot be reached.
	 */
	public abstract bool perform(GameObject agent);

	/**
	 * Does this action need to be within range of a target game object?
	 * If not then the moveTo state will not need to run for this action.
	 */
	public abstract bool requiresInRange ();
	

	/**
	 * Are we in range of the target?
	 * The MoveTo state will set this and it gets reset each time this action is performed.
	 */
	public bool isInRange () {
        bool returnVal = false;
        
        try
        {
            if (target == null)
                return true;


            if (!requiresInRange())
            {
                returnVal = true; // inRange not requried so return true
            }
            else if (inRangeProximty == 0 && target != null && transform.position.Equals(target.sensedObject.transform.position))
            {
                returnVal = true; // exact proximity found
            }
            else if (target != null) // check proximty
            {
                returnVal = (Vector3.Distance(transform.position, target.sensedObject.transform.position) <= inRangeProximty);
            }

        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }
        finally
        {
        }
        return returnVal;

	}

	public void addPrecondition(string key, object value) {
		preconditions.Add (new KeyValuePair<string, object>(key, value) );
	}


	public void removePrecondition(string key) {
		KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
		foreach (KeyValuePair<string, object> kvp in preconditions) {
			if (kvp.Key.Equals (key)) 
				remove = kvp;
		}
		if ( !default(KeyValuePair<string,object>).Equals(remove) )
			preconditions.Remove (remove);
	}


	public void addEffect(string key, object value) {
		effects.Add (new KeyValuePair<string, object>(key, value) );
	}


	public void removeEffect(string key) {
		KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
		foreach (KeyValuePair<string, object> kvp in effects) {
			if (kvp.Key.Equals (key)) 
				remove = kvp;
		}
		if ( !default(KeyValuePair<string,object>).Equals(remove) )
			effects.Remove (remove);
	}

	
	public HashSet<KeyValuePair<string, object>> Preconditions {
		get {
			return preconditions;
		}
	}

	public HashSet<KeyValuePair<string, object>> Effects {
		get {
			return effects;
		}
	}
}