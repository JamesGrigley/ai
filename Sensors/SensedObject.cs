﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SensedObject
{
    public GameObject sensedObject;
    public float knowTime;
    public float sightTime;

    public bool triggerAlert = false;

    private Vector3 _knownPosition;
    public Vector3 knownPosition
    {
        get
        {
            return _knownPosition;
        }
        set
        {
            knowTime = Time.time;
            _knownPosition = value;
        }
    }
    private int _canSeeStatusID = -1; // -1 never saw, 0 = can't see, 1 = can see
    public bool canSee
    {
        get
        {
            return _canSeeStatusID == 1;
        }
        set
        {
            if (_canSeeStatusID == -1) // alert because we previously didn't know targer
            {
                triggerAlert = true;
            }
            else if (_canSeeStatusID == 0 && value == true) // we couldn't see him, but now we can... so alert
            {
                triggerAlert = true;
            }

            if (value == true){
                sightTime = Time.time;
                _canSeeStatusID = 1;
            }
            else
            {
                _canSeeStatusID = 0;
            }
        }
    }

    public SensedObject()
    {

    }

    public SensedObject(GameObject _sensedObject)
    {
        sensedObject = _sensedObject;
        knownPosition = _sensedObject.transform.position;
    }


    public SensedObject(GameObject _sensedObject, bool _canSee)
    {
        sensedObject = _sensedObject;
        knownPosition = _sensedObject.transform.position;
        canSee = _canSee;
    }
}

