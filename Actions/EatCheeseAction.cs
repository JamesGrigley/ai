﻿using UnityEngine;
using System.Collections;

public class EatCheeseAction : GoapAction {

    private bool ateCheese = false;

    public EatCheeseAction()
    {
        actionName = "EatCheeseAction";

        addPrecondition("findCheese", true);

        addEffect("eatCheese", true);
        addEffect("findCheese", false);
	}

    public override void reset()
    {
        Destroy(target.sensedObject);
        ateCheese = false;
    }

    public override bool isDone()
    {
        return ateCheese;
    }

    public override bool requiresInRange()
    {
        return true;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        //Debug.Log("checkProceduralPrecondition eat cheese");
        return true;
    }

    public override bool perform(GameObject agent)
    {
        //Debug.Log("perform eat cheese");
        //
        ateCheese = true;
        return true;
    }

    

}
