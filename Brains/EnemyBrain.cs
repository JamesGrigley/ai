﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBrain : BrainBase, IGoap
{
    private GoapAgent goapAgent; // this reference is so brain can interrupt the goap agent
    private Vector3 gizmoTarget; // for debugging
    private bool hasTarget = false;
    private NPCController npcController;

    void Start()
    {
        goapAgent = GetComponent<GoapAgent>();
        npcController = GetComponent<NPCController>();
    }

    public bool moveAgent(GoapAction nextAction)
    {
        //Debug.Log(gameObject.name + ": moveAgent for " + nextAction.actionName + "; Distance to target: " + Vector3.Distance(transform.position, nextAction.target.sensedObject.transform.position).ToString());

        bool inRange = nextAction.isInRange();

        if (!inRange)
        {
            npcController.moveTo(nextAction.targetPosition);
        }

        // if we return false this (moveAgent) method will be called again
        //Debug.Log("inRange && nextAction.target.canSee: " + (inRange && nextAction.target.canSee));
        return inRange && nextAction.target.canSee;
    }

    public void stopAgent()
    {
        npcController.stop();
    }

    // our senses are alerting us to an object
    public override void receiveAlertFromSenses(GameObject alertGameObject)
    {
        //Debug.Log(gameObject.name + ": EnemyBrain.receiveAlertFromSenses");
        if (!senseManager.knowsTarget(alertGameObject))
        {
            goapAgent.interrupt();
            squad.alert(alertGameObject, getPosition(), (IBrain)this);
        }

    }

    // our squad is alerting us to an object
    public override void receiveAlertFromSquad(GameObject alertGameObject)
    {
        Debug.Log(gameObject.name + ": EnemyBrain.receiveAlertFromSquad");

        if (!senseManager.knowsTarget(alertGameObject))
        {
            senseManager.receiveAlert(alertGameObject);
            goapAgent.interrupt();
        }
    }

    public void receiveDamageAlert(GameObject alertGameObject) // from living entity
    {
        Debug.Log(gameObject.name + ": receiveDamageAlert");

        senseManager.receiveAlert(alertGameObject);
        goapAgent.interrupt();
    }

    public void receiveDealthAlert(GameObject killedBy) // from living entity
    {
        //Debug.Log("oh shit, I was killed by " + killedBy.name + " :( ");

        squad.alertDeath(this, killedBy);

    }

    public override void receiveCommand(string command, Vector3 target)
    {
        Debug.Log("Command to " + command + " " + target + " received.");

        if (command == "charge")
        {
            GetComponent<GoToTargetAction>().nonObjectTarget = target;
            hasTarget = true;
            goapAgent.interrupt();
        }
    }



    /**
     * Key-Value data that will feed the GOAP actions and system while planning.
     */
    public HashSet<KeyValuePair<string, object>> getWorldState()
    {
        SensedObject closestKnowTarget = senseManager.getClosestKnownTarget();
        HashSet<KeyValuePair<string, object>> worldData = new HashSet<KeyValuePair<string, object>>();

        bool knowsTarget = false;
        bool canSeeTarget = false;
        bool canMelee = true;//for now (transform.FindChild("Sword") != null && transform.FindChild("Sword").gameObject.activeSelf);
        bool canShoot = (transform.FindChild("Bow") != null && transform.FindChild("Bow").gameObject.activeSelf);
        bool isSafe = gameObject.GetComponent<LivingEntity>().Health > (gameObject.GetComponent<LivingEntity>().maxHealth * .5);


        if (closestKnowTarget != null)
        {
            knowsTarget = true;
            canSeeTarget = closestKnowTarget.canSee;
        }

        worldData.Add(new KeyValuePair<string, object>("knowsTarget", knowsTarget));
        worldData.Add(new KeyValuePair<string, object>("canSeeTarget", canSeeTarget));
        worldData.Add(new KeyValuePair<string, object>("isSafe", isSafe));
        worldData.Add(new KeyValuePair<string, object>("hasTarget", hasTarget));
        worldData.Add(new KeyValuePair<string, object>("canMelee", canMelee));
        worldData.Add(new KeyValuePair<string, object>("canShoot", canShoot));
        //if (knowsTarget)
        //    Debug.Log(gameObject.name + ": getWorldState: knowsTarget=" + knowsTarget.ToString() + "; canSeetarget=" + canSeeTarget.ToString() + "; isSafe=" + isSafe.ToString());
        return worldData;
    }

    public void planFailed(HashSet<KeyValuePair<string, object>> failedGoal)
    {
        // Not handling this here since we are making sure our goals will always succeed.
        // But normally you want to make sure the world state has changed before running
        // the same goal again, or else it will just fail.
       // Debug.Log(gameObject.name + ": <color=red>Plan failed</color>");

    }

    public void planFound(HashSet<KeyValuePair<string, object>> goal, Queue<GoapAction> actions)
    {
        // we found a plan for our goal
        //Debug.Log(gameObject.name + ": <color=green>Plan found</color> " + GoapAgent.prettyPrint(actions));
    }

    public void actionsFinished()
    {

        //Debug.Log(gameObject.name + ": <color=blue>Actions completed</color>");
    }

    public void planAborted(GoapAction aborter)
    {
        // An action bailed out of the plan. State has been reset to plan again.
        // Take note of what happened and make sure if you run the same goal again
        // that it can succeed.
        //Debug.Log(gameObject.name + ": <color=red>Plan Aborted</color> " + GoapAgent.prettyPrint(aborter));
    }

    public void planInterrupted(string by = "")
    {
        if (by != "")
            Debug.Log(gameObject.name + ": <color=red>Plan Interrupted by " + by + "</color>");
        else
            Debug.Log(gameObject.name + ": <color=red>Plan Interrupted</color>");
    }
}
