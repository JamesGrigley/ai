﻿using UnityEngine;
using System.Collections;

public class FollowAction : GoapAction {

    private bool doneFollowing = false;

    public FollowAction()
    {
        actionName = "FollowAction";

		//addPrecondition ("knowsTarget", false);
        addEffect("followPlayer", true);
        
	}

    public override void reset()
    {
        //target = null;
        doneFollowing = false;
    }

    public override bool isDone()
    {
        return doneFollowing;
    }

    public override bool requiresInRange()
    {
        return true; // no, 
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {

        target = new SensedObject(GameObject.Find("Target"));

        if (target != null && !isInRange())
        {
            return true;
        }
        else
            return false;
    }

    public override bool perform(GameObject agent)
    {
        //Debug.Log("follow perform");

        doneFollowing = true;
        return true;
    }
}
