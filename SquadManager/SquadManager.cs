﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SquadManager : MonoBehaviour
{
    List<Squad> squads;

    void Awake()
    {
        squads = new List<Squad>();
        createSquad("Enemy");
        createSquad("Friendly");

    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void createSquad(string tag)
    {
        Squad squad = new Squad(tag);

        // squad needs to know the brains in it
        if (tag == "Friendly") // this is hard coded for now
        {
            squad.brains = getBrainsForFriendlySquad();
        }
        else
        {
            squad.brains = getBrainsForSquad(tag);
        }


        squads.Add(squad);

        //Debug.Log("Assigning brains");
        // brains need to know which squad they belong to
        foreach (IBrain brain in squad.brains)
        {
            brain.assignSquad(squad);
        }

    }

    private List<IBrain> getBrainsForFriendlySquad() // for lack of a better idea for now
    {
        List<IBrain> brains = new List<IBrain>();

        foreach (IBrain brain in getBrainsForSquad("Player"))
        {
            brains.Add(brain);
        }

        foreach (IBrain brain in getBrainsForSquad("Companion"))
        {
            brains.Add(brain);
        }

        return brains;
    }

    private List<IBrain> getBrainsForSquad(string tag)
    {
        List<IBrain> brains = new List<IBrain>();
        GameObject[] agents = GameObject.FindGameObjectsWithTag(tag);

        foreach (GameObject agent in agents)
        {
            //Debug.Log("found " + agent.name + " for squad.");

            foreach (Component comp in agent.GetComponents(typeof(Component)))
            {
                //Debug.Log("found " + comp.GetType() + " component attached to agent.");

                if (typeof(IBrain).IsAssignableFrom(comp.GetType()))
                {
                    //Debug.Log("found " + comp.GetType().ToString() + " component to connect squad.");

                    IBrain brain = (IBrain)comp;

                    brains.Add(brain);

                    //Debug.Log("Assigned " + agent.name + " to " + tag + " squad.");
                }
            }
        }
        return brains;
    }
    
}
