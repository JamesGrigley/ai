﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleeAction : GoapAction {

    private bool doneFleeing = false;
    public SenseManager sensor;

    public FleeAction()
    {
        actionName = "FleeAction";

		addPrecondition ("isSafe", false);

        addEffect("fleeTarget", true);
        requiresObjectTarget = false;
	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
    }

    public override void reset()
    {
        doneFleeing = false;
    }

    public override bool isDone()
    {
        return doneFleeing;
    }

    public override bool requiresInRange()
    {
        return true;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        setFleePosition();

        return true;
    }

    public override bool perform(GameObject agent)
    {
        //GetComponent<MouseBrain>().SetMoveSpeed(2);

        //doneFleeing = true;
        return true;
    }

    private void setFleePosition()
    {
        //List<Vector3> possiblePositions = new List<Vector3>();

        //for (int x = 0; x <= 10; x++)
        //{

        //}
        //Debug.Log(sensor.getFleeDirection());
        //Quaternion rotation = Quaternion.Euler(sensor.getFleeDirection());
        //transform.rotation = transform.rotation * rotation;


        //transform.rotation = target.sensedObject.transform.rotation * Quaternion.Euler(0, Random.Range(0, 180), 0);

        nonObjectTarget = transform.position + (transform.forward * -30);
        //Debug.Log(nonObjectTarget);
        //

        //target = new SensedObject(new GameObject("Random Position"));
        //target.sensedObject.transform.position = transform.position + (transform.forward * 15);
        //GetComponent<EnemyBrain>().SetMoveSpeed(7);

    }
}
