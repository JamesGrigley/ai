﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SenseManager : MonoBehaviour {

    //private BrainBase brain;
    private SensedObjectManager sensedObjectManager;

    void Awake()
    {
        sensedObjectManager = GetComponent<SensedObjectManager>();
        
    }


    void Start()
    {
        //brain = GetComponent<BrainBase>();

    }

    public SensedObject getClosestKnownTarget(bool canSee = false)
    {
        return sensedObjectManager.getClosestKnownTarget(canSee);
    }

    public bool knowsTarget(GameObject target)
    {
        return sensedObjectManager.knowsTarget(target);
    }

    // senses will call this
    public bool updateSensedObject(GameObject sensedObject, bool canSee = false)
    {
        bool updated = false;
        //Debug.Log("sensedObject.name: " + sensedObject.name);
        //Debug.Log("sensedObject.GetComponent<BrainBase>().squad.name: " + sensedObject.GetComponent<BrainBase>().squad.name);

        if (sensedObject != null)
        {
            updated = sensedObjectManager.Add(sensedObject, canSee);
        }

        return updated;
    }

    // sensed object manager will call this
    public void receiveAlertFromOM(GameObject alertObject, bool canSee = false)
    {
        GetComponent<BaseNPC>().senseTarget(alertObject, canSee);
        //brain.receiveAlertFromSenses(alertObject);
    }

    // squad manager will call this
    public void receiveAlert(GameObject sensedObject)
    {
        sensedObjectManager.Add(sensedObject);
    }

    public bool canSeeTarget(GameObject target)
    {
        return sensedObjectManager.canSeeTarget(target);
    }    

    public Vector3 getFleeDirection()
    {
        return sensedObjectManager.getFleeDirection();
    }
}

