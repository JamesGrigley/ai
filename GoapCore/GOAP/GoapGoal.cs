using UnityEngine;
using System.Collections.Generic;

public abstract class GoapGoal : MonoBehaviour {

    public float priority;
    public string goalName;
    public bool goalState;


    public HashSet<KeyValuePair<string, object>> GoalHash
    {
        get
        {
            HashSet<KeyValuePair<string, object>> goal = new HashSet<KeyValuePair<string, object>>();
            goal.Add(new KeyValuePair<string, object>(goalName, goalState));

            return goal;
        }
    }
}