﻿using UnityEngine;
using System.Collections;

public class EatCheeseGoal : GoapGoal {

    public EatCheeseGoal()
    {
        goalName = "eatCheese";
        goalState = true;
	}
}
