﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class SearchGrid : MonoBehaviour
{

    private int maxNodeSearchDistance = 40;
    Heap<Node> nodeHeap;
    private Grid masterGrid;
    Node startNode;
    Node targetNode;

    Vector3 targetPosition;

    public List<Node> waypoints;
    public List<Node> debugAIPoints;

    public SearchGrid(Vector3 startPos, Vector3 _targetPos, Grid _masterGrid)
    {
        targetPosition = _targetPos;
        waypoints = new List<Node>();
        debugAIPoints = new List<Node>();
        masterGrid = _masterGrid;
        startNode = masterGrid.NodeFromWorldPoint(startPos);
        targetNode = masterGrid.NodeFromWorldPoint(targetPosition);
        nodeHeap = new Heap<Node>((int)_masterGrid.gridWorldSize.x * (int)_masterGrid.gridWorldSize.y * (int)_masterGrid.gridWorldSize.z);
        nodeHeap.Add(startNode);

        loadSeachGrid();
        //resetHuristic();
        retracePath();
    }

    private void loadSeachGrid()
    {
        int x = 0;
        Node lastNode = null;

        while (x < 5000)
        {
            x++;
            Node nodeToExpand = nodeHeap.RemoveFirst();

            if (nodeToExpand == null || nodeToExpand == targetNode)
            {
                if (nodeToExpand != null)
                {
                    targetNode.parentNode = lastNode;
                }

                break;
            }

            //Debug.Log("Expanding " + nodeToExpand.gridVector + " with cost of " + nodeToExpand.costVector);
            //Add neighbors to the heap with scores
            //waypoints.Add(nodeToExpand);//for debugging the A* search
            expandNode(nodeToExpand);
            nodeToExpand.extended = true;
            lastNode = nodeToExpand;
            debugAIPoints.Add(nodeToExpand);
        }
        //Debug.Log("loadSeachGrid() complete at " + x);
    }

    private void retracePath()
    {
        List<Node> waypointsTemp = new List<Node>();
        Node lastNode = targetNode;

        lastNode.worldPosition = targetPosition; // we want to go after the actual location of the target, not its node point

        int x = 0;
        while (x < 5000)
        {
            x++;
            Node nextNode = lastNode.getBestNeighbor();

            if (nextNode == null || nextNode == startNode)
                break;

            waypointsTemp.Add(lastNode);

            lastNode = nextNode;


        }

        waypointsTemp.Reverse();

        waypoints = waypointsTemp;


    }

    private void expandNode(Node node)
    {
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    Node nextNeighbor = masterGrid.GetNode(node.gridX + x, node.gridY + y, node.gridZ + z);

                    if (nextNeighbor == null || nextNeighbor == node)
                        continue;

                    // we can only go up a level if the neighbor is low enough
                    if (y == 1)
                    {
                        //Debug.Log("Vertical distance of " + distance);
                        if (nextNeighbor.worldPosition.y - node.worldPosition.y > .9f)
                        {
                            continue;
                        }
                    }

                    nextNeighbor.gCost = node.gCost + 1;
                    nextNeighbor.hCost = GetDistance(nextNeighbor, targetNode);

                    if (nextNeighbor.walkable)
                    {

                        if (!nodeHeap.Contains(nextNeighbor))
                        {
                            // link the nodes so I can retrace back through them
                            //node.neighborNodes.Add(nextNeighbor);
                            //nextNeighbor.neighborNodes.Add(node);
                            nextNeighbor.parentNode = node;

                            nodeHeap.Add(nextNeighbor);
                        }
                        //Debug.Log("Found neighbor " + nextNeighbor.gridVector + " with cost of " + nextNeighbor.costVector);

                    }
                }
            }
        }
    }

    public Vector3[] toVectorArray()
    {
        Vector3[] vectors = new Vector3[waypoints.Count];

        for (int x = 0; x < waypoints.Count; x++)
        {
            vectors[x] = waypoints[x].worldPosition;
        }
        return vectors;
    }

    private int GetDistance(Node nodeA, Node nodeB) {

        //return (int)Vector3.Distance(nodeA.worldPosition, nodeB.worldPosition) * 100;
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstZ = Mathf.Abs(nodeA.gridZ - nodeB.gridZ);

        //if (dstX > dstZ)
        //    return 14 * dstZ + 10 * (dstX - dstZ);
        //return 14 * dstX + 10 * (dstZ - dstX);

        return (dstX + dstZ) * 2;
	}

}

