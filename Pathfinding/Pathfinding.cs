﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Pathfinding : MonoBehaviour {


	PathRequestManager requestManager;

    public Transform redCube;
    public Transform greenCube;
    private float cubeTime = 1;

	void Awake() {
		requestManager = GetComponent<PathRequestManager>();

	}
	

	public void StartFindPath(Vector3 startPos, Vector3 targetPos) {

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();

        StopCoroutine(FindPath(startPos, targetPos));
        //Debug.Log("StartFindPath startPos: " + startPos + "; targetPos: " + targetPos);
        StartCoroutine(FindPath(startPos, targetPos));

        sw.Stop();
        Debug.Log("Search grid in " + sw.ElapsedMilliseconds + " milliseconds");
	}

    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {
        SearchGrid searchGrid = new SearchGrid(startPos, targetPos, GetComponent<Grid>());

        foreach (Node node in searchGrid.debugAIPoints)
        {
            cubeTime = cubeTime + .01f;
            StartCoroutine(showCube(node, cubeTime, redCube, 1));
        }
        yield return new WaitForSeconds(1);
        foreach (Node node in searchGrid.waypoints)
        {
            cubeTime = cubeTime + .05f;
            StartCoroutine(showCube(node, cubeTime, greenCube, 0));
        }

        yield return null;
        requestManager.FinishedProcessingPath(searchGrid.toVectorArray(), true);
    }

    // for debugging
    IEnumerator showCube(Node _nodeToExpand, float _cubeTime, Transform _greenCube, float destroyTime)
    {
        if (greenCube != null)
        {
            yield return new WaitForSeconds(_cubeTime);
            Transform cube = Instantiate(_greenCube, _nodeToExpand.worldPosition, default(Quaternion)) as Transform;
            if (destroyTime > 0)
                GameObject.Destroy(cube.gameObject, destroyTime);
            //Debug.Log("Expanding " + _nodeToExpand.gridVector + " with cost of " + _nodeToExpand.costVector);
        }
    }

    // this is not in use, but we may need something like this at some point
	Vector3[] SimplifyPath(List<Node> path) {
		List<Vector3> waypoints = new List<Vector3>();
		Vector2 directionOld = Vector2.zero;

		for (int i = 1; i < path.Count; i ++) {
			Vector2 directionNew = new Vector2(path[i-1].gridX - path[i].gridX, path[i-1].gridZ - path[i].gridZ);

            if (directionNew != directionOld)
            {
                waypoints.Add(path[i].worldPosition);
            }
            directionOld = directionNew;
		}
		return waypoints.ToArray();
	}
}
