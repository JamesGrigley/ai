﻿using UnityEngine;
using System.Collections;

public class SafeController : NPCController
{

    // Use this for initialization
    void Start()
    {
        animateIdle();
    }

    public override void animateIdle()
    {
       // Debug.Log("idle animation");
        animateStop();
    }
    public override void animateMove()
    {
        //Debug.Log("starting animation");
        animator.SetBool("Pursuing", true);
        //animator.SetFloat("Speed", moveSpeed);
        animator.SetBool("Hiding", false);
    }

    public override void animateStop()
    {
        //Debug.Log("stopping animation");
        animator.SetBool("Pursuing", false);
        //animator.SetFloat("Speed", 0);
        animator.SetBool("Hiding", true);
    }

    public override void animateMelee()
    {
        //Debug.Log("melee animation");
        animateMove();
        animator.SetTrigger("Attack");
    }

}
