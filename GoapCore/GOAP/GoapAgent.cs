﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;


public sealed class GoapAgent : MonoBehaviour {

	private FSM stateMachine;

	private FSM.FSMState idleState; // finds something to do
	private FSM.FSMState moveToState; // moves to a target
	private FSM.FSMState performActionState; // performs an action
	
	private HashSet<GoapAction> availableActions;
    private List<GoapGoal> availableGoals;
    
	private Queue<GoapAction> currentActions;

	private IGoap dataProvider; // this is the implementing class that provides our world data and listens to feedback on planning

	private GoapPlanner planner;

    private bool isInterrupted = false;

    public string debugCurrentState = "Started";


	void Start () {
        

		stateMachine = new FSM ();
		availableActions = new HashSet<GoapAction> ();
        availableGoals = new List<GoapGoal>();
		currentActions = new Queue<GoapAction> ();
		planner = new GoapPlanner ();
		findDataProvider ();
        loadGoals();
		createIdleState ();
		createMoveToState ();
		createPerformActionState ();
		stateMachine.pushState (idleState);

        loadActions();
	}
	

	void Update () {
		stateMachine.Update (this.gameObject);
	}


	public void addAction(GoapAction a) {
		availableActions.Add (a);
	}

	public GoapAction getAction(Type action) {
		foreach (GoapAction g in availableActions) {
			if (g.GetType().Equals(action) )
			    return g;
		}
		return null;
	}

	public void removeAction(GoapAction action) {
		availableActions.Remove (action);
	}

	private bool hasActionPlan() {
		return currentActions.Count > 0;
	}

	private void createIdleState() {
		idleState = (fsm, gameObj) => {
            //Debug.Log("createIdleState");
            debugCurrentState = "Idle State";
            // get the world state
            HashSet<KeyValuePair<string, object>> worldState = dataProvider.getWorldState();

            // loop through goals, they are already sorted by priority
            foreach (GoapGoal goal in availableGoals)
            {
                //Debug.Log("goal.goalName: " + goal.goalName);

                // Try to Plan
                Queue<GoapAction> plan = planner.plan(gameObject, availableActions, worldState, goal.GoalHash);

                if (plan != null)
                {
                    // plan found
                    currentActions = plan;
                    dataProvider.planFound(goal.GoalHash, plan);

                    fsm.popState(); // move to PerformAction state
                    fsm.pushState(performActionState);

                    return;

                }
                else
                {
                    dataProvider.planFailed(goal.GoalHash);
                }
            }

            //Debug.Log("Repop idel state");
            fsm.popState(); // move back to IdleAction state
            fsm.pushState(idleState);
			

		};
	}
	
	private void createMoveToState() {
		moveToState = (fsm, gameObj) => {
            //Debug.Log("createMoveToState");
            debugCurrentState = "Move State";

			// move the game object

            // if we are interrupted
            if (isInterrupted) //
            {
                Debug.Log("MoveTo interupted");
                isInterrupted = false;
                fsm.popState();
                fsm.pushState(idleState);
                dataProvider.planInterrupted("createMoveToState");
                return;
            }

			GoapAction action = currentActions.Peek();
			if (action.requiresObjectTarget && action.target == null) {
                Debug.Log("<color=red>Fatal error:</color> " + action.actionName + " requires a target but has none. Planning failed. You did not assign the target in your Action.checkProceduralPrecondition()");
				fsm.popState(); // move
				fsm.popState(); // perform
				fsm.pushState(idleState);
				return;
			}

            // get the agent to move itself
			if ( dataProvider.moveAgent(action) ) {
                dataProvider.stopAgent();
				fsm.popState();
			}

		};
	}
	
	private void createPerformActionState() {

		performActionState = (fsm, gameObj) => {
            //Debug.Log("createPerformActionState");
            debugCurrentState = "Perform State";

            //if we are interrupted
            if (isInterrupted) //
            {
               // Debug.Log("createPerformActionState isInterrupted");
                isInterrupted = false;
                fsm.popState();
                fsm.pushState(idleState);
                dataProvider.planInterrupted("createPerformActionState");
                return;
            }

			// perform the action

			if (!hasActionPlan()) {
                Debug.Log("!hasActionPlan");
				// no actions to perform
				Debug.Log("<color=red>Done actions</color>");
				fsm.popState();
				fsm.pushState(idleState);
				dataProvider.actionsFinished();
				return;
			}

			GoapAction action = currentActions.Peek();
			if ( action.isDone() ) {
				// the action is done. Remove it so we can perform the next one
                //Debug.Log("action.isDone");
				currentActions.Dequeue();
			}

			if (hasActionPlan()) {
				// perform the next action
				action = currentActions.Peek();

				if ( action.isInRange()) {
					// we are in range, so perform the action
					bool success = action.perform(gameObj);

					if (!success) {
						// action failed, we need to plan again
						fsm.popState();
						fsm.pushState(idleState);
						dataProvider.planAborted(action);
					}
				} else {
					// we need to move there first
					// push moveTo state
					fsm.pushState(moveToState);
				}

			} else {
				// no actions left, move to Plan state
				fsm.popState();
				fsm.pushState(idleState);
				dataProvider.actionsFinished();
			}

		};
	}

	private void findDataProvider() {
		foreach (Component comp in gameObject.GetComponents(typeof(Component)) ) {
			if ( typeof(IGoap).IsAssignableFrom(comp.GetType()) ) {
				dataProvider = (IGoap)comp;
				return;
			}
		}
	}

	private void loadActions ()
	{
		GoapAction[] actions = gameObject.GetComponents<GoapAction>();
		foreach (GoapAction a in actions) {
			availableActions.Add (a);
		}
		//Debug.Log("Found actions: "+prettyPrint(actions));
	}

    private void loadGoals()
    {
        GoapGoal[] goals = gameObject.GetComponents<GoapGoal>();

        IEnumerable<GoapGoal> query = goals.OrderByDescending(goal => goal.priority);

        foreach(GoapGoal goal in query){
            availableGoals.Add(goal);
        }
    }

    public void interrupt()
    {
        isInterrupted = true;
    }

	public static string prettyPrint(HashSet<KeyValuePair<string,object>> state) {
		String s = "";
		foreach (KeyValuePair<string,object> kvp in state) {
			s += kvp.Key + ":" + kvp.Value.ToString();
			s += ", ";
		}
		return s;
	}

	public static string prettyPrint(Queue<GoapAction> actions) {
		String s = "";
		foreach (GoapAction a in actions) {
			s += a.GetType().Name;
			s += "-> ";
		}
		s += "GOAL";
		return s;
	}

	public static string prettyPrint(GoapAction[] actions) {
		String s = "";
		foreach (GoapAction a in actions) {
			s += a.GetType().Name;
			s += ", ";
		}
		return s;
	}

	public static string prettyPrint(GoapAction action) {
		String s = ""+action.GetType().Name;
		return s;
	}
}
