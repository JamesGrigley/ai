﻿using UnityEngine;
using System.Collections;

public class IdleGoal : GoapGoal
{

    public IdleGoal()
    {
        goalName = "idle";
        goalState = true;
	}
}
