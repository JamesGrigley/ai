﻿using UnityEngine;
using System.Collections;

public class BulletOLD : MonoBehaviour {
    public float damage = 10;
    public GameObject shooter;
    private bool hit = false;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 1);
        GetComponent<Rigidbody>().velocity = transform.forward * 75;
	}
	
	// Update is called once per frame
	void Update () {
        //transform.Translate(transform.right);
	}

    void OnCollisionEnter(Collision col)
    {
        if (!hit)
        {
            hit = true;
            //Debug.Log("I hit " + col.gameObject.name);

            LivingEntity npc = col.gameObject.GetComponent<LivingEntity>();

            if (npc != null && GetComponent<BrainBase>() != null && GetComponent<BrainBase>().squad != col.gameObject.GetComponent<BrainBase>().squad)
            {
                npc.Damage(10, (int)(DAMAGE_TYPES.TYPE_PHYSICAL | DAMAGE_TYPES.TYPE_POISON | DAMAGE_TYPES.TYPE_BLEED), shooter);
            }
            Destroy(gameObject);

        }
    }
}
