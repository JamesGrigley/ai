﻿using UnityEngine;
using System.Collections;

public class ButterflyController : MonoBehaviour
{
    private Animator animator;

    private GameObject[] flowers;

    private Transform targetFlower;

    private float distanceToTarget;

    private int statusID = 0; // 0-do nothing // 1=eating // 2 = flying // 3 = landing

    private float speed = 0.5f;

    Vector3 startPos;
    Vector3 endPos;
    float startTime = 0;
    float journeyLength = 0;
    float trajectoryHeight = 0.5f;


    void Awake()
    {
        animator = transform.GetChild(0).GetComponent<Animator>();
    }

    void Start()
    {
        flowers = GameObject.FindGameObjectsWithTag("Flower");
        Debug.Log("Flowers: " + flowers.Length.ToString());
        gotoRandomFlower();
    }

    void Update()
    {
        if(statusID == 2)
        {
            //transform.LookAt(targetFlower.position);
            //transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

            move();


            if (Vector3.Distance(transform.position, targetFlower.position) < 0.1)
            {
                setSpeed(0.5f);
                statusID = 3; // landing
            }
        }
        else if (statusID == 3)
        {
            //transform.LookAt(targetFlower.position);
            //transform.Translate(transform.forward * speed * 0.1f * Time.deltaTime, Space.World);
            move();
            if (Vector3.Distance(transform.position, targetFlower.position) < 0.02)
            {
                setSpeed(0.1f);
                statusID = 1; // eating
                StartCoroutine(delayNextFlower());
            }
        }

        Debug.DrawLine(transform.position, targetFlower.position, Color.red);

    }

    void move()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;

        // calculate current time within our lerping time range
        //float cTime = Time.time * 0.2f;


        // calculate straight-line lerp position:
        Vector3 currentPos = Vector3.Lerp(startPos, endPos, fracJourney);
        // add a value to Y, using Sine to give a curved trajectory in the Y direction
        currentPos.y += trajectoryHeight * Mathf.Sin(Mathf.Clamp01(fracJourney) * Mathf.PI);
        // finally assign the computed position to our gameObject:
        transform.position = currentPos;
    }

    void gotoRandomFlower()
    {
        int flowerIndex = Random.Range(0, flowers.Length - 1);

        if (flowers[flowerIndex].transform != targetFlower)
        {
            targetFlower = flowers[flowerIndex].transform;
            startPos = transform.position;
            endPos = targetFlower.position;
            journeyLength = Vector3.Distance(transform.position, targetFlower.position) + 2;
            trajectoryHeight = Random.Range(0.2f, 0.5f);
            startTime = Time.time;
            setSpeed(1.0f);
            transform.LookAt(targetFlower.position);
            statusID = 2;
        }
    }

    void setSpeed(float speed)
    {
        animator.SetFloat("Speed", speed);
    }

    IEnumerator delayNextFlower()
    {
        yield return new WaitForSeconds(Random.Range(2, 7));
        gotoRandomFlower();

    }
}
