﻿using UnityEngine;
using System.Collections;

public class ShootAction : GoapAction {

    private bool shot = false;

    public Transform bullet;
    public SenseManager sensor;

    public float shotDelay = 1f;
    private float lastShot = Mathf.NegativeInfinity;
    private bool shooting = false;

    public ShootAction()
    {
        actionName = "ShootAction";

        addPrecondition("canSeeTarget", true);
        addPrecondition("isSafe", true);
        addPrecondition("canShoot", true);

        addEffect("killTarget", true);
	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
    }


    public override void reset()
    {
        //Destroy(target);
        shot = false;
    }

    public override bool isDone()
    {
        return shot;
    }

    public override bool requiresInRange()
    {
        return true;
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        //Debug.Log(gameObject.name + ": Shoot checkProceduralPrecondition");

        SensedObject sensedTarget = sensor.getClosestKnownTarget(false); // we don't need to see them yet

        if (sensedTarget != null)
            target = sensedTarget;

        //return target != null;
        return true;
    }

    public override bool perform(GameObject agent)
    {
        Debug.Log(gameObject.name + ": Shoot perform");
        return shoot();
    }

    private bool shoot()
    {
        if (Time.time < (lastShot + shotDelay))
        {
            // can't shoot yet
            //Debug.Log("Can't shoot yet");
            return true; //because we didn't fail, but shot remains false
        }
        else if (target.sensedObject != null && target.canSee)
        {
            shooting = true;

            transform.LookAt(target.sensedObject.transform.position);

            Transform bulletInstance = Instantiate(bullet, (transform.position + (transform.forward * 2)), transform.rotation) as Transform;
            bulletInstance.GetComponent<Bullet>().shooter = gameObject;

            blowBack();
            GetComponent<AudioSource>().Play();
            lastShot = Time.time;
            return true; // return true without shot = true to keep shooting
        }
        else
        {
            shot = true; //this will end the shooting action
            return false; 
        }
    }

    private void blowBack()
    {
        transform.GetChild(0).localPosition += Vector3.back * 0.05f;
        StartCoroutine(blowBackreturn());
    }

    IEnumerator blowBackreturn()
    {
        yield return new WaitForSeconds(0.05f);
        //Debug.Log ("blowBackreturn");

        transform.GetChild(0).localPosition += Vector3.forward * 0.05f;
    }

    

}
