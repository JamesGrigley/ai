﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Squad
{
    public List<IBrain> brains;
    public float commRadius = 30;
    public string name = "";

    public Squad(string tag)
    {
        name = tag;
        brains = new List<IBrain>();

        
    }

    public bool alert(GameObject alertGameObject, Vector3 alerterPosition, IBrain senderBrain)
    {
        foreach (IBrain brain in brains)
        {
            Debug.Log("alerting a brain");
            if(brain != null && brain != senderBrain && Vector3.Distance(alerterPosition, brain.getPosition()) <= commRadius)
            {
                brain.receiveAlertFromSquad(alertGameObject);
            }
        }
        return true;
    }

    public void alertDeath(EnemyBrain brain, GameObject killedBy)
    {
        brains.Remove(brain);
        if (killedBy != null)
            alert(killedBy, killedBy.transform.position, null);
    }

    public void receiveCommand(string command, Vector3 target, IBrain senderBrain)
    {
        foreach (IBrain brain in brains)
        {
            //Debug.Log("alerting a brain");
            if (brain != null && brain != senderBrain)
            {
                brain.receiveCommand(command, target);
            }
        }
    }
}
