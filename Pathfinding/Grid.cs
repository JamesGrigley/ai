﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Grid : MonoBehaviour {

	public bool displayGridGizmos;
	public Vector3 gridWorldSize;
	public float nodeRadius;

	Node[,,] grid;
    int unwalkableMask;

	float nodeDiameter;
	int gridSizeX, gridSizeY, gridSizeZ;
    private Vector3 offset;

	void Awake() {
        int LayerID = 10;
        unwalkableMask = (1 << LayerID);
        nodeDiameter = nodeRadius*2;
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x/nodeDiameter);
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y/nodeDiameter);
        gridSizeZ = Mathf.RoundToInt(gridWorldSize.z/nodeDiameter);

        offset = new Vector3(transform.position.x - (gridWorldSize.x / 2), transform.position.y - (gridWorldSize.y / 2), transform.position.z - (gridWorldSize.z / 2));

        CreateGrid();
	}

	public int MaxSize {
		get {
            return gridSizeX * gridSizeY * gridSizeZ;
		}
	}

    public Node GetNode(int x, int y, int z)
    {
        Node node = null;
        try
        {
            node = grid[x, y, z];
        }
        catch (Exception ex)
        {//
            //Debug.Log(ex.Message + " at " + new Vector3(x, y, z).ToString());
        }
        return node;
    }

	void CreateGrid() {
        grid = new Node[gridSizeX, gridSizeY, gridSizeZ];
        for (int z = 0; z < gridSizeZ; z++) {
            for (int y = 0; y < gridSizeY; y ++) {
                for (int x = 0; x < gridSizeX; x++)
                {

                    Vector3 worldPoint = offset + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius) + Vector3.forward * (z * nodeDiameter + nodeRadius);

                    bool walkable = IsWalkable(ref worldPoint);

                    grid[x, y, z] = new Node(walkable, worldPoint, x, y, z);
                }
			}
		}
	}

    private bool IsWalkable(ref Vector3 worldPoint)
    {
        RaycastHit hit;

        //Debug.DrawRay(worldPoint + Vector3.up * nodeRadius, -Vector3.up * nodeDiameter, Color.cyan, 100);

        // Send a ray down to snap node to terrain
        if (Physics.Raycast(worldPoint + Vector3.up * nodeRadius, -Vector3.up, out hit, nodeDiameter, ~unwalkableMask))
        {
            worldPoint.y = hit.point.y;
            //return true;
            if (checkpoints(worldPoint))
            {
                return true;
            }
        }

        return false;
    }

    private bool checkpoints(Vector3 worldPoint) 
    {
        //Debug.DrawRay(worldPoint + new Vector3(-nodeRadius, 0, -nodeRadius), Vector3.up * nodeDiameter * 2, Color.cyan, 100);
        //Debug.DrawRay(worldPoint + new Vector3(nodeRadius, 0, -nodeRadius), Vector3.up * nodeDiameter * 2, Color.cyan, 100);
        //Debug.DrawRay(worldPoint + new Vector3(nodeRadius, 0, nodeRadius), Vector3.up * nodeDiameter * 2, Color.cyan, 100);
        //Debug.DrawRay(worldPoint + new Vector3(-nodeRadius, 0, nodeRadius), Vector3.up * nodeDiameter * 2, Color.cyan, 100);
        //


        if (
            Physics.Raycast(worldPoint + new Vector3(-nodeRadius, 0, -nodeRadius), Vector3.up * nodeDiameter * 2, nodeDiameter, unwalkableMask)// top left (-1, -1)
            || Physics.Raycast(worldPoint + new Vector3(nodeRadius, 0, -nodeRadius), Vector3.up * nodeDiameter * 2, nodeDiameter, unwalkableMask) //top right (+1, -1)
            || Physics.Raycast(worldPoint + new Vector3(nodeRadius, 0, nodeRadius), Vector3.up * nodeDiameter * 2, nodeDiameter, unwalkableMask) // bottom right +1, +1
            || Physics.Raycast(worldPoint + new Vector3(-nodeRadius, 0, nodeRadius), Vector3.up * nodeDiameter * 2, nodeDiameter, unwalkableMask) // bottom left -1, +1
            || Physics.Raycast(worldPoint + Vector3.up * nodeRadius, Vector3.up * nodeDiameter * 2)
            )
        {
            return false;

        }

        else
            return true;
        
    }

	public Node NodeFromWorldPoint(Vector3 worldPosition) {

        //Debug.Log("worldPosition: " + worldPosition);


        //Debug.Log("offsetX: " + offset.x + "; offsetY: " + offset.y + "; offsetZ: " + offset.z);

        float percentX = (worldPosition.x - offset.x) / nodeDiameter;
        float percentY = (worldPosition.y - offset.y) / nodeDiameter;
        float percentZ = (worldPosition.z - offset.z) / nodeDiameter;

        //Debug.Log("percentX: " + percentX + "; percentY: " + percentY + "; percentZ: " + percentZ);

        //percentX = Mathf.Clamp01(percentX);
        //percentY = Mathf.Clamp01(percentY);
        //percentZ = Mathf.Clamp01(percentZ);

        //Debug.Log("percentX: " + percentX + "; percentY: " + percentY + "; percentZ: " + percentZ);


        int x = Mathf.RoundToInt(percentX) - 1;
        int y = Mathf.RoundToInt(percentY) - 1;
        int z = Mathf.RoundToInt(percentZ) - 1;

        if (y < 0)
            y = 0;

        //Debug.Log("x: " + x + "; y: " + y + "; z: " + z);
        
        return grid[x, y, z];
	}
	
	void OnDrawGizmos() {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, gridWorldSize.z));
		if (grid != null && displayGridGizmos) {
			foreach (Node n in grid) {
				Gizmos.color = (n.walkable)?Color.black:Color.red;
                //Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - .1f));
                if (n.walkable)// && Vector3.Distance(GameObject.Find("Mimic Safe").transform.position, n.worldPosition) < 10)
                {
                    //Gizmos.DrawCube(n.worldPosition, Vector3.one * nodeDiameter/10);
                    Gizmos.DrawCube(n.worldPosition, new Vector3(nodeDiameter * .8f, nodeDiameter/10, nodeDiameter * .8f));
                }
			}
		}
	}
}