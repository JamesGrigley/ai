﻿using UnityEngine;
using System.Collections;

public class Brute : BaseNPC
{
    protected override void Awake()
    {
        base.Awake();
        walkSpeed = 2;
        isHostile = true;
    }

    public override void Start()
    {
        base.Start();
    }


    public override void hit(int combo)
    {
        Debug.Log("Hit()");
        animator.SetInteger("HitCount", combo);
        animator.SetTrigger("Hit");

        navMeshAgent.speed = 0;
        animator.SetFloat("speed", 0);
        StartCoroutine(endHit());
    }

    IEnumerator endHit()
    {
        yield return new WaitForSeconds(1);
        navMeshAgent.speed = walkSpeed;
        animator.SetFloat("Speed", 1);

    }

    public override void die()
    {
        animator.SetTrigger("die");
        navMeshAgent.Stop();
        GetComponent<BoxCollider>().enabled = false;
    }

    override public void attack()
    {
        animator.SetFloat("Speed", 0);
        animator.SetBool("Combat", true);
        animator.SetInteger("HitCount", 1);
        animator.SetTrigger("Hit");        
        transform.LookAt(attackingTarget.transform.position);
        StartCoroutine(endAttack());
    }


    IEnumerator endAttack()
    {
        yield return new WaitForSeconds(2);

        isAttacking = false;
    }
}