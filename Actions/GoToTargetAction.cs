﻿using UnityEngine;
using System.Collections;

public class GoToTargetAction : GoapAction {

    private bool atTarget = false;
    private SenseManager sensor;

    public GoToTargetAction()
    {
        actionName = "GoToTargetAction";

        addPrecondition("hasTarget", true);

        addEffect("goToTarget", true);

        requiresObjectTarget = false;

	}

    void Start()
    {
        sensor = GetComponent<SenseManager>();
    }

    public override void reset()
    {

        atTarget = false;
    }

    public override bool isDone()
    {
        return atTarget;
    }

    public override bool requiresInRange()
    {
        return true; 
    }

    public override bool checkProceduralPrecondition(GameObject agent)
    {
        //target = sensor.getClosestKnownTarget().sensedObject;
        //Debug.Log("target != null " + (target != null));

        return true;
    }

    public override bool perform(GameObject agent)
    {
        //Debug.Log("goto perform");

        //atTarget = true;
        return true;
    }
}
