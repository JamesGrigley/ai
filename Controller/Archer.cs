﻿using UnityEngine;
using System.Collections;

public class Archer : BaseNPC
{

    float holdSpeed;

    Arrow arrowPrefab;
    Arrow arrow;
    public Transform arrowSlot;

    protected override void Awake()
    {
        base.Awake();
        attackDistance = 10;
        arrowPrefab = Resources.Load<Arrow>("Prefabs/Weapons/Arrow");
        isHostile = true;

    }

    public override void Start()
    {
        base.Start();
    }

    override public void attack()
    {
        transform.LookAt(attackingTarget.transform.position);
        animator.SetFloat("Speed", 0);
        animator.SetTrigger("Shoot Arrow");
        soundManager.playAfterDelay(SoundClipTypeEnum.BowPull, transform, 1);

        StartCoroutine(endAttack());
    }

    protected override void alert(bool canSee = false)
    {
        animator.SetBool("alert", true);
    }


    IEnumerator endAttack()
    {
        yield return new WaitForSeconds(1.0f);

        arrow = (Arrow)Instantiate(arrowPrefab, arrowSlot.position, arrowSlot.rotation);
        arrow.transform.SetParent(arrowSlot);

        yield return new WaitForSeconds(0.5f);

        soundManager.play(SoundClipTypeEnum.BowRelease, transform);

        arrow.transform.LookAt(attackingTarget.transform.position + Vector3.up * 1.5f);
        arrow.addForce();
        yield return new WaitForSeconds(2);
        isAttacking = false;
    }


    //public override void hit(float force)
    //{
    //    Debug.Log("Hit()");
    //    animator.SetTrigger("impact");
    //    holdSpeed = navMeshAgent.speed;
    //    navMeshAgent.speed = 0;
    //    animator.SetFloat("Speed", 0);
    //    StartCoroutine(endHit());
    //}

    //IEnumerator endHit()
    //{
    //    yield return new WaitForSeconds(1);
    //    navMeshAgent.speed = holdSpeed;
    //    animator.SetFloat("Speed", 1);

    //}

    //public override void die()
    //{
    //    animator.SetTrigger("die");
    //    navMeshAgent.Stop();
    //    GetComponent<BoxCollider>().enabled = false;
    //}




}